# c-quine

From [Wikipedia](https://en.wikipedia.org/wiki/Quine_(computing))

> A quine is a computer program that takes no input and produces a copy of its own source code as its only output. The standard terms for these programs in the computability theory and computer science literature are "self-replicating programs", "self-reproducing programs", and "self-copying programs".

C-Quine is just my attempt to write a quine in C, for fun. =)

To test:

```bash
$ make quine
```

will generate the executable. Then you can do:

```bash
$ ./quine > output.c
$ diff quine.c output.c
```

the `diff` command should produce no output (meaning that the code of the
program and the generated output of the program are equal).
