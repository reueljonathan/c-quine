#include <stdio.h>

void parse_no_escape(char * dest, char * str) {
    int offset = 0;
    while (*str) {
        if (*str == '"') { dest[offset++] = '\\'; dest[offset++] = '"'; }
        else if (*str == '\n') { dest[offset++] = '\\'; dest[offset++] = 'n'; }
        else if (*str == '\t') { dest[offset++] = '\\'; dest[offset++] = 't'; }
        else if (*str == '\\') { dest[offset++] = '\\'; dest[offset++] = '\\'; }
        else { dest[offset++] = *str; }
        str++;
    }
    dest[offset] = '\0';
}

int main(void) {
    char src[1500] = "#include <stdio.h>\n\nvoid parse_no_escape(char * dest, char * str) {\n    int offset = 0;\n    while (*str) {\n        if (*str == '\"') { dest[offset++] = '\\\\'; dest[offset++] = '\"'; }\n        else if (*str == '\\n') { dest[offset++] = '\\\\'; dest[offset++] = 'n'; }\n        else if (*str == '\\t') { dest[offset++] = '\\\\'; dest[offset++] = 't'; }\n        else if (*str == '\\\\') { dest[offset++] = '\\\\'; dest[offset++] = '\\\\'; }\n        else { dest[offset++] = *str; }\n        str++;\n    }\n    dest[offset] = '\\0';\n}\n\nint main(void) {\n    char src[1500] = \"%s\";\n    char no_escapes[1500];\n    parse_no_escape(no_escapes, src);\n\n    printf(src, no_escapes);\n    return 0;\n}\n";
    char no_escapes[1500];
    parse_no_escape(no_escapes, src);

    printf(src, no_escapes);
    return 0;
}
